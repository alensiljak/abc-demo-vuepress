# abc-demo

Demonstration of various features of ABC music notation using abcjs library.

## Introduction

This is a web site which should demonstrate various features available in ABC music notation. It uses the excellent abcjs library for that purpose.

## Development

`yarn dev docs`

## Deployment

The site is intended to run on GitLab Pages.
