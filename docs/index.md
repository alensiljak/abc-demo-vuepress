---
home: true
layout: HomeLayout
#heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline: Demonstration of various features of ABC music notation using abcjs library.
actionText: Quick Start →
actionLink: /guide/
features:
- title: Feature 1 Title
  details: Feature 1 Description
- title: Feature 2 Title
  details: Feature 2 Description
- title: Feature 3 Title
  details: Feature 3 Description
footer: Made by Alen Šiljak with ❤️
---
This is index page

Go to [Demo1](demos/demo1.md)
